from digi.xbee.devices import XBeeDevice
from digi.xbee.io import IOLine, IOMode
from time import sleep

module = XBeeDevice("/dev/ttyUSB0", 9600)
control_device_id = "CONTROL"
ep_device_id = "CONTROL"
module.open()
xbee_network = module.get_network()
control_device = xbee_network.discover_device(control_device_id)
ep_device = xbee_network.discover_device(ep_device_id)


if control_device is None:
    print("Could not find the remote module")
    exit(1)
if ep_device is None:
    print("Could not find the remote module")
    exit(1)

def data_received(xbee_message):
    data = str(xbee_message.data.decode())
    print('message received:', data)
    input_to_send = str("message received")
    print("Sending data to %s : %s..." % (ep_device.get_64bit_addr(), input_to_send))
    module.send_data(ep_device, input_to_send)

module.add_data_received_callback(data_received)

input()

